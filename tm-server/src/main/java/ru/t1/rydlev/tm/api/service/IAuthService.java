package ru.t1.rydlev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.model.Session;
import ru.t1.rydlev.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws Exception;

    @NotNull
    Session validateToken(@Nullable String token) throws Exception;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    void logout(@Nullable Session session) throws Exception;

}
