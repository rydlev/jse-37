package ru.t1.rydlev.tm.endpoint;

import io.swagger.annotations.ApiParam;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.endpoint.IAuthEndpoint;
import ru.t1.rydlev.tm.api.service.IServiceLocator;
import ru.t1.rydlev.tm.dto.Result;
import ru.t1.rydlev.tm.dto.request.UserLoginRequest;
import ru.t1.rydlev.tm.dto.request.UserLogoutRequest;
import ru.t1.rydlev.tm.dto.request.UserViewProfileRequest;
import ru.t1.rydlev.tm.dto.response.UserLoginResponse;
import ru.t1.rydlev.tm.dto.response.UserLogoutResponse;
import ru.t1.rydlev.tm.dto.response.UserViewProfileResponse;
import ru.t1.rydlev.tm.exception.EndpointException;
import ru.t1.rydlev.tm.model.Session;
import ru.t1.rydlev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api/AuthEndpoint")
@WebService(endpointInterface = "ru.t1.rydlev.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }


    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/ping")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Result ping() {
        return new Result();
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/loginUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserLoginResponse loginUser(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        try {
            @NotNull final String token = getServiceLocator().getAuthService().login(request.getLogin(), request.getPassword());
            return new UserLoginResponse(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }

    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/logoutUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserLogoutResponse logoutUser(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        @NotNull final Session session = check(request);
        try {
            getServiceLocator().getAuthService().logout(session);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new UserLogoutResponse();
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/viewUserProfile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserViewProfileResponse viewUserProfile(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable User user;
        try {
            user = getServiceLocator().getUserService().findOneById(userId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new UserViewProfileResponse(user);
    }

}
