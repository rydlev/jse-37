package ru.t1.rydlev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.model.User;

import java.sql.ResultSet;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User fetch(@NotNull ResultSet row) throws Exception;

    @NotNull
    User add(@NotNull User user) throws Exception;

    void update(@NotNull User user) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @Nullable String email
    ) throws Exception;

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @Nullable Role role
    ) throws Exception;

    @Nullable
    User findByLogin(@NotNull String login) throws Exception;

    @Nullable
    User findByEmail(@NotNull String email) throws Exception;

    Boolean isLoginExists(@NotNull String login) throws Exception;

    Boolean isEmailExists(@NotNull String email) throws Exception;

}
