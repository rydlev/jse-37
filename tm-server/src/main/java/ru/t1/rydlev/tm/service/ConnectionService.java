package ru.t1.rydlev.tm.service;

import lombok.SneakyThrows;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.api.service.IConnectionService;
import ru.t1.rydlev.tm.api.service.IPropertyService;

import java.sql.Connection;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final BasicDataSource dataSource = dataSource();

    public ConnectionService() {
    }

    @NotNull
    @Override
    @SneakyThrows
    public BasicDataSource dataSource() {
        @NotNull final String url = propertyService.getDBUrl();
        @NotNull final String username = propertyService.getDBUser();
        @NotNull final String password = propertyService.getDBPassword();
        @NotNull final BasicDataSource result = new BasicDataSource();
        result.setUrl(url);
        result.setUsername(username);
        result.setPassword(password);
        result.setMinIdle(5);
        result.setMaxIdle(10);
        result.setAutoCommitOnReturn(false);
        result.setMaxOpenPreparedStatements(100);
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final Connection connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

}