package ru.t1.rydlev.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskCompleteByIndexResponse extends AbstractTaskResponse {
}
