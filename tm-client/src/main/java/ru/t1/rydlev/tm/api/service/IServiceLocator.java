package ru.t1.rydlev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IAuthEndpoint getAuthEndpointClient();

    @NotNull
    IProjectEndpoint getProjectEndpointClient();

    @NotNull
    ISystemEndpoint getSystemEndpointClient();

    @NotNull
    ITaskEndpoint getTaskEndpointClient();

    @NotNull
    IUserEndpoint getUserEndpointClient();

}
