package ru.t1.rydlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.UserLogoutRequest;
import ru.t1.rydlev.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        getAuthEndpoint().logoutUser(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Logout current user.";
    }

    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
